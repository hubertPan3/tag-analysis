package com.icarusfrog.tagAnalysis.services;

import com.icarusfrog.tagAnalysis.domain.mongo.MongoArticle;
import com.icarusfrog.tagAnalysis.exceptions.ArticleNotFoundException;
import com.icarusfrog.tagAnalysis.repositories.mongo.MongoArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ArticleStorageManager {
    private MongoArticleRepository mongoArticleRepository;

    @Autowired
    public ArticleStorageManager(MongoArticleRepository mongoArticleRepository){
        this.mongoArticleRepository = mongoArticleRepository;
    }

    public MongoArticle storeArticle(MongoArticle mongoArticle){
        return this.mongoArticleRepository.save(mongoArticle);
    }

    public List<MongoArticle> storeArticles(Collection<MongoArticle> mongoArticleCollection){
        return this.mongoArticleRepository.saveAll(mongoArticleCollection);
    }

    public MongoArticle getArticleById(Long id) throws ArticleNotFoundException {
        Optional<MongoArticle> possibleMongoArticle = this.mongoArticleRepository.findById(id);
        if(possibleMongoArticle.isPresent()){
            return possibleMongoArticle.get();
        } else {
            throw new ArticleNotFoundException();
        }
    }


}
