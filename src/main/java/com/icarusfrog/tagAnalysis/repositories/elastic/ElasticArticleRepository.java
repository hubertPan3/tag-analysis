package com.icarusfrog.tagAnalysis.repositories.elastic;

import com.icarusfrog.tagAnalysis.domain.elastic.ElasticArticle;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ElasticArticleRepository extends PagingAndSortingRepository<ElasticArticle, Long> {
}
