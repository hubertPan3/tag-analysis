package com.icarusfrog.tagAnalysis.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.icarusfrog.tagAnalysis.repositories.elastic")
@EnableMongoRepositories(basePackages = "com.icarusfrog.tagAnalysis.repositories.mongo")
public class DataRepositoryConfig {
}
