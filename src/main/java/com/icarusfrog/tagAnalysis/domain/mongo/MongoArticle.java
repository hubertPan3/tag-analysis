package com.icarusfrog.tagAnalysis.domain.mongo;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Builder
@Data
@Document
public class MongoArticle {
    @Id
    Long id;
    String title;
    String body;
    List<String> tags;
}
