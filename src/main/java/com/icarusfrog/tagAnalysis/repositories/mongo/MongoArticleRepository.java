package com.icarusfrog.tagAnalysis.repositories.mongo;

import com.icarusfrog.tagAnalysis.domain.mongo.MongoArticle;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoArticleRepository extends MongoRepository<MongoArticle, Long> {
}
