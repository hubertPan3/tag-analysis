package com.icarusfrog.tagAnalysis.domain.elastic;

import java.util.List;

public class ElasticArticle {
    Long id;
    String title;
    String body;
    List<String> tags;
}
