package com.icarusfrog.tagAnalysis.controllers;

import com.icarusfrog.tagAnalysis.domain.mongo.MongoArticle;
import com.icarusfrog.tagAnalysis.exceptions.ArticleNotFoundException;
import com.icarusfrog.tagAnalysis.services.ArticleStorageManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@Slf4j
@RequestMapping(value="/storage")
@RestController
public class StorageController {

    private ArticleStorageManager articleStorageManager;

    @Autowired
    public StorageController(ArticleStorageManager articleStorageManager){
        this.articleStorageManager = articleStorageManager;
        MongoArticle dummyMongoArticle = MongoArticle.builder().body("demo body").title("demo title").id(1l).tags(Arrays.asList("demo")).build();
        articleStorageManager.storeArticle(dummyMongoArticle);
    }

    @GetMapping(value="/{id}")
    public MongoArticle getArticleById(@PathVariable Long id) throws ArticleNotFoundException {
        MongoArticle mongoArticle = articleStorageManager.getArticleById(id);
        log.debug("Returning MongoArticle: " +  mongoArticle.toString());
        System.out.println("Returning MongoArticle: " +  mongoArticle.toString());
        return mongoArticle;
    }

    @PutMapping("/")
    public MongoArticle insertMongoArticle(@RequestBody MongoArticle mongoArticle){
        log.debug("Inserting mongo article: " + mongoArticle.toString());
        return articleStorageManager.storeArticle(mongoArticle);
    }
}
