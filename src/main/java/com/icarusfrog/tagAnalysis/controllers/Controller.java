package com.icarusfrog.tagAnalysis.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
public class Controller {

    @RequestMapping("/")
    public String index(HttpServletResponse response){
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return "{message: 'Hello, world!'}";
    }
}
