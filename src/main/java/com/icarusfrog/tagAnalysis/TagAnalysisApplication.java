package com.icarusfrog.tagAnalysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
public class TagAnalysisApplication {

	public static void main(String[] args) {
		SpringApplication.run(TagAnalysisApplication.class, args);
	}

}
